@extends('layout.master')
@section('judul')
Create New Account
@endsection

@section('content')
    <form action="/welcome" method="post">
      @csrf
      <label>First Name</label><br>
        <input type="text" name="first" required><br><br>
      <label>Last Name</label><br>
        <input type="text" name="last"><br><br>
      <label>Gender :</label> <br>
        <input type="radio" name="Gender" required value="Male">Male<br>
        <input type="radio" name="Gender" required value="Female">Female<br> <br>
      <label>Nationality</label> <br>
      <select name="Nationality" required>
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapore">Singapore</option><br>
      </select> <br> <br>
      <label>Language Spoken</label> <br>
        <input type="checkbox" name="Language" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="Language" value="English">English<br>
        <input type="checkbox" name="Language" value="Sunda">Sunda<br> <br>
      <label>Bio</label> <br>
        <textarea name="Bio" rows="10" cols="30" required></textarea> <br>
        <input type="submit" value="Sign Up">
        <input type="reset" value="Reset">
        <input type="button" value="Cancel" onclick="history.back(-1)">
      </form>
@endsection
