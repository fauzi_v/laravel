<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
      return view('halaman.register');
    }
    public function welcome(Request $request)
    {
      // dd($request->all());
      $first_name = $request['first'];
      $last_name = $request['last'];

      return view('halaman.welcome', compact('first_name','last_name'));
    }
}
